<?php

namespace backend\controllers;

use common\models\GoodsPhotos;
use common\models\UploadPhoto;
use Yii;
use common\models\Goods;
use common\models\search\Goods as GoodsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\UploadedFile;

/**
 * GoodsController implements the CRUD actions for Goods model.
 */
class GoodsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Goods models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GoodsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Goods model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Goods model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Goods();
        $orderId = Yii::$app->request->get('order_id');
        $model->order_id = $orderId;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/order/update', 'id' => $orderId]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Goods model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        /** @var Goods $model */
        $model = $this->findModel($id);
        $model->order_id = Yii::$app->request->get('order_id');
        // Сохраняем товар и переходим обратно к форме редактирования заказа.
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /** Удаление фотографии из товара
     * @param integer $id  id Фото товара
     * @return \yii\web\Response
     */
    public function actionPhotoRemove($id)
    {
        /** @var  GoodsPhotos $model */
        $model = GoodsPhotos::find()->where(['id' => $id])->one();
        $product = $model->product;
        if(!empty($model)) {
            $filepath = yii::$app->params['photoPath'] . $model->filename;
            if (file_exists($filepath)) {
                unlink($filepath);
            }
            $model->delete();
            Yii::$app->session->setFlash('success', 'Фотография успешно удалена');
            return $this->redirect('/order/update?id=' . $product->order_id);
        }
        throw new BadRequestHttpException('Передано неверное значение статуса участника');
    }


    /** Добавить фото к товару
     * @param integer $id
     * @return \yii\web\Response
     */
    public function actionPhotoAdd($id)
    {
        $images = UploadedFile::getInstancesByName('UploadForm[photo]');
        $productId = (int)Yii::$app->request->post('id');
        if(!empty($images)){
            foreach ($images as $image){
                $model = new GoodsPhotos();
                $uploadForm = new UploadPhoto();
                $uploadForm->image = $image;
                $model->product_id = $productId;
                $model->filename = $uploadForm->uploadImage();
                if(!empty($model->filename)){
                    $model->save();
                }else{
                    foreach ($uploadForm->errors['image'] as $error)
                    Yii::$app->session->setFlash('danger', $error);
                }
            }
            Yii::$app->session->setFlash('success', 'Фотографии успешно сохранены');
        }
        return $this->redirect('/order/update?id='.$id);
    }

    /**
     * Deletes an existing Goods model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Goods model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Goods the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Goods::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
