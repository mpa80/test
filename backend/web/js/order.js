jQuery(function() {
    'use strict';
    $('.upload-photos-link').on('click', function(){
        $('#modal-upload-photos').modal('show');
        $('.modal-upload-photos-id').val($(this).data('id'));
        return false;
    });
});