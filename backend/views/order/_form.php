<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;
use \yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model common\models\Orders */
/* @var $form yii\widgets\ActiveForm */
/* @var $dataProvider yii\data\ActiveDataProvider*/

// $this->registerJsFile('/js/order.js');
?>

<div class="orders-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'customer_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?php if(!$model->isNewRecord){ ?>
        <?php // Теперь список товаров со всеми фото размещаем?>
        <div>Товары в заказе:</div>
        <?=
        ListView::widget([
            'dataProvider' => $dataProvider,
            'summary' => false,
            'itemOptions' => ['class' => 'product'],
            'itemView' => '_product',
        ])
        ?>
        
        <?=Html::a('Добавить товар', '/goods/create?order_id='.$model->id)?>
    <?php } ?>



    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php Modal::begin([
    'header' => '<h2>Загрузка фото</h2>',
    'id' => 'modal-upload-photos',
    //'toggleButton' => ['label' => 'Х'],
    //'footer' => '',
])?>
<?=Html::beginForm(['goods/photo-add', 'id'=>$model->id], 'post', ['enctype' => 'multipart/form-data'])?>
<?=Html::fileInput('UploadForm[photo][]', '', ['multiple'=>'multiple', 'class'=>'upload-photos'])?>
<?=Html::hiddenInput('id', '', ['class'=>'modal-upload-photos-id'])?>
<?=Html::submitButton('Загрузить')?>
<?=Html::endForm()?>
<?php Modal::end();?>
