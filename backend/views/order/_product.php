<?php
    use \yii\helpers\Html;
    use common\models\GoodsPhotos;
    /** @var common\models\Goods $model */
?>

<div>Артикул:<?=$model->id?>| <strong><?=$model->name?></strong></div>

<div>
    <?php /** @var GoodsPhotos $photo */
    foreach ($model->goodsPhotos as $photo){ ?>
        <span><?=Html::img('/photos/'. $photo->filename, ['width'=>'200'])?><?=Html::a('Удалить', '/goods/photo-remove?id='.$photo->id)?></span>
    <?php } ?>
</div>
<div><?=Html::a('Добавить фото', '#'.$model->id, ['data-id'=>$model->id, 'class'=>'upload-photos-link'])?></div>
<div><?=$model->desc?></div>
<hr/>
