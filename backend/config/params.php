<?php
return [
    'adminEmail' => 'admin@example.com',
    'baseUrl' =>  '/',//'/admin',
    'photoPath' => Yii::getAlias('@backend'). DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'photos' . DIRECTORY_SEPARATOR,
];
