<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%orders}}`.
 */
class m191019_144529_create_orders_table extends Migration
{
    private $table = 'orders';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%orders}}', [
            'id' => $this->primaryKey(),
            'customer_name' => $this->string(255),
            'address' => $this->string(255),
            'description' => $this->string(255),
        ]);
        $this->addCommentOnColumn($this->table, 'description', 'Дополнительная информация к заказу');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%orders}}');
    }
}
