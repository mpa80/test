<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%goods__photos}}`.
 */
class m191019_144555_create_goods__photos_table extends Migration
{
    private $table = 'goods__photos';
    public function safeUp()
    {
        $this->createTable('{{%goods__photos}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(11),
            'filename' => $this->string(255),
        ]);

        $this->createIndex('product_id_'.$this->table.'_idx', $this->table, 'product_id');
        $this->addForeignKey('product_id_'.$this->table.'_fk', $this->table, 'product_id', 'goods', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('product_id_'.$this->table.'_fk', $this->table);
        $this->dropIndex('product_id_'.$this->table.'_idx', $this->table);
        $this->dropTable('{{%goods__photos}}');
    }
}
