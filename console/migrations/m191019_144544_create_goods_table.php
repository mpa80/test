<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%goods}}`.
 */
class m191019_144544_create_goods_table extends Migration
{
     private $table = 'goods';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%goods}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(11),
            'name' => $this->string(255),
            'desc' => $this->string(255),
        ]);

        $this->createIndex('order_id_'.$this->table.'_idx', $this->table, 'order_id');
        $this->addForeignKey('order_id_'.$this->table.'_fk', $this->table, 'order_id', 'orders', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('order_id_'.$this->table.'_fk', $this->table);
        $this->dropIndex('order_id_'.$this->table.'_idx', $this->table);
        $this->dropTable('{{%goods}}');
    }
}
