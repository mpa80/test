<?php


namespace frontend\widgets\feedback;

use yii\base\Widget;

class FeedbackWidget extends Widget
{
    public $model;

    public function run()
    {
        return $this->render('index', [
            'model' => $this->model
        ]);
    }

}