<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Форма обратной связи';
$this->params['breadcrumbs'][] = $this->title;

\yii\web\YiiAsset::register($this);
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'feedback-form']); ?>
            <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>
            <?= $form->field($model, 'phone') ?>
            <?= $form->field($model, 'email') ?>
            <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>

            <div class="form-group">
                <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<div class="row col-lg-12">
    <img src="/113.jpg" width="200" height="200" alt="Sample" usemap="#sample">
    <map name="sample" class="aaa">
        <area shape="poly" alt="111" coords="30,30,120,180,180,50,50,30,30" class="poly" href="#">
    </map>
</div>
