<?php
return [
    'adminEmail' => 'admin@example.com',
    'adminName' => 'admin',
    'supportEmail' => 'support@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'user.passwordResetTokenExpire' => 3600,
    'EMAIL_ACCOUNT_HOST' => '',
    'EMAIL_ACCOUNT_PORT' => 587,
    'EMAIL_ACCOUNT_USER' => '',
    'EMAIL_ACCOUNT_PASSWORD' => '',
];
