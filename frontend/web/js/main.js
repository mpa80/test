jQuery(function(){
    "use strict";
    $('#feedback-form').on('beforeSubmit', function () {
        let $yiiform = $(this);
        $.ajax({
                type: $yiiform.attr('method'),
                url: $yiiform.attr('action'),
                data: $yiiform.serializeArray(),
            }
        )
        .done(function(data) {
            if(data.success) {
                // Всё норм, выводим сообщалку, можно модалку вывести, но я не стал уже
                window.console.log('Всё норм');
            } else if (data.validation) {
                // проверка полей провалилась
                $yiiform.yiiActiveForm('updateMessages', data.validation, true);
            } else {
                // ошибка отправки письма
                window.console.log('Сообщение не отправилось');
            }
        })
        .fail(function () {
            // запрос не удался
        })

        return false;
    })
});