<?php


namespace common\models;


use yii\base\Model;

class UploadPhoto extends Model
{
    public $icon, $image;

    public function rules()
    {
        return [
            [['image'], 'file', 'skipOnEmpty' => true, 'maxFiles' => 10, 'maxSize' => 5120000, 'extensions' => 'jpeg, jpg, png, bmp, gif, svg, tiff', 'checkExtensionByMimeType' => false],
        ];
    }

    public function uploadImage($name = null)
    {
        //if ($this->validate()) {
            if (!empty($this->image)) {
                $path = \yii::$app->params['photoPath'];
                $filename = time() . '-'. $this->image->name . '.'. $this->image->extension;
                if (!empty($name)) {
                    $filename = time() . '-'.$name . '.' . $this->image->extension;
                }
                $filepath = $path . $filename;
                $this->image->saveAs($filepath);
                return $filename;
            }
        //}
        return null;
    }
}