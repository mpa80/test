<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "goods__photos".
 *
 * @property int $id
 * @property int $product_id
 * @property string $filename
 *
 * @property Goods $product
 */
class GoodsPhotos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'goods__photos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id'], 'integer'],
            [['filename'], 'string', 'max' => 255],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Goods::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'идентификатор товара',
            'filename' => 'Имя файла фотографии',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Goods::className(), ['id' => 'product_id']);
    }
}
